# tus-webhook

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm]

Express middleware for parsing tus webhook.

## Installation

```bash
npm i tus-webhook
```

## Usage

```typescript
import express, { Express } from "express";
import bodyParser from "body-parser";

import { handleTusdHook, Tusd, TusdRequest } from "tus-webhook";

const app = express();
app.use(bodyParser.json());
app.post("/hook", handleTusdHook, (req, res) => {
  const { tusd } = req as TusdRequest;
  // Do something with tusd
  res.json({});
});
```

[license]: https://gitlab.com/joshua-avalon/tus-webhook/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/joshua-avalon/tus-webhook/pipelines
[pipelines_badge]: https://gitlab.com/joshua-avalon/tus-webhook/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/joshua-avalon/tus-webhook/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/tus-webhook
[npm_badge]: https://img.shields.io/npm/v/tus-webhook/latest.svg
