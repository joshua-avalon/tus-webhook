import tus from "tus-js-client";
import fs from "fs";
import { Server } from "http";
import { Express } from "express";

import { Tusd } from "@/index";
import { isHookName } from "@/middleware";

import { appFactory } from "./express";

const endpoint = process.env.TUSD_ENDPOINT || "";
jest.setTimeout(30000);

const filename = "testing";

function upload(): Promise<void> {
  return new Promise(resolve => {
    fs.readFile(__filename, (_, data: any) => {
      const upload = new tus.Upload(data, {
        endpoint,
        metadata: { filename },
        onSuccess: () => resolve(),
        onError: () => resolve()
      });
      upload.start();
    });
  });
}

describe("tusd integration", () => {
  let app: Express;
  let server: Server;
  const mock = jest.fn();

  beforeEach(async done => {
    mock.mockReset();
    app = appFactory(mock);
    server = app.listen(3000, async () => {
      await upload();
      done();
    });
  });
  test("parsing webhook", async done => {
    server.close(() => {
      expect(mock).toHaveBeenCalledTimes(4);
      mock.mock.calls.forEach((args: any[]) => {
        const tusd: Tusd | undefined = args[0];
        expect(tusd).toBeDefined();
        if (!tusd) {
          return;
        }
        expect(isHookName(tusd.name)).toBe(true);
        expect(tusd.hook.metaData.filename).toBe(filename);
      });
      done();
    });
  });
});
