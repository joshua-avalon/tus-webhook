import express, { Express } from "express";
import bodyParser from "body-parser";

import { handleTusdHook, Tusd, TusdRequest } from "@/index";

export const appFactory = (onRequest: (tusd?: Tusd) => void): Express => {
  const app = express();
  app.use(bodyParser.json(), handleTusdHook);
  app.post("/", (req, res) => {
    const { tusd } = req as TusdRequest;
    onRequest(tusd);
    res.json({});
  });
  return app;
};
