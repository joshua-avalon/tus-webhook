# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.2]

### Changed

- Update dependencies.

## [1.0.1]

### Changed

- Update dependencies.

## 1.0.0

### Added

- Initial Release.

[unreleased]: https://gitlab.com/joshua-avalon/tus-webhook/compare/1.0.2...master
[1.0.2]: https://gitlab.com/joshua-avalon/tus-webhook/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.com/joshua-avalon/tus-webhook/compare/1.0.0...1.0.1
