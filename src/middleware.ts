import { NextFunction, Request, Response } from "express";
import { toTusdHook, TusdHookName, TusdRequest } from "./model";

export function isHookName(name: string = ""): name is TusdHookName {
  return [
    "pre-create",
    "post-create",
    "post-finish",
    "post-terminate",
    "post-receive"
  ].includes(name);
}

export function handleTusdHook(
  request: Request & TusdRequest,
  _: Response,
  next: NextFunction
): void {
  const name = request.header("Hook-Name");
  if (!isHookName(name)) {
    next();
    return;
  }
  const hook = toTusdHook(request.body);
  if (!hook) {
    next();
    return;
  }
  request.tusd = {
    name,
    hook
  };
  next();
}
