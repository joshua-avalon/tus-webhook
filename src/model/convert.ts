import Ajv from "ajv";

import hookSchema from "./hook.schema.json";
import { TusdHook } from "./tusd";
import { TusdJson } from "./json";

export function toTusdHook(data: any): TusdHook | null {
  const ajv = new Ajv();
  const validate = ajv.compile(hookSchema);
  const valid = validate(data);
  if (!valid) {
    return null;
  }
  const json: TusdJson = data;
  const {
    ID: id,
    Size: size,
    Offset: offset,
    IsFinal: isFinal,
    IsPartial: isPartial,
    PartialUploads: partialUploads,
    MetaData: metaData
  } = json;
  return {
    id,
    size,
    offset,
    isFinal,
    isPartial,
    partialUploads,
    metaData
  };
}
