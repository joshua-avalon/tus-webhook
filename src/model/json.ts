export type TusdJson = {
  /**
   * The upload's ID. Will be empty during the pre-create event.
   */
  ID: string;
  /**
   * The upload's total size in bytes.
   */
  Size: number;
  /**
   * The upload's current offset in bytes.
   */
  Offset: number;
  /**
   * These properties will be set to true, if the upload as a final or partial one. See the Concatenation extension for details: http://tus.io/protocols/resumable-upload.html#concatenation
   */
  IsFinal: boolean;
  /**
   * These properties will be set to true, if the upload as a final or partial one. See the Concatenation extension for details: http://tus.io/protocols/resumable-upload.html#concatenation
   */
  IsPartial: boolean;
  /**
   * If the upload is a final one, this value will be an array of upload IDs which are concatenated to produce the upload.
   */
  PartialUploads: string[];
  /**
   * The upload's meta data which can be supplied by the clients as it wishes. All keys and values in this object will be strings. Be aware that it may contain maliciously crafted values and you must not trust it without escaping it first!
   */
  MetaData: {
    [key: string]: string;
  };
  [key: string]: any;
};
