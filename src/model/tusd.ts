export type TusdRequest = {
  tusd?: Tusd;
};

export type Tusd = {
  hook: TusdHook;
  name: TusdHookName;
};

export type TusdHookName =
  | "pre-create"
  | "post-create"
  | "post-finish"
  | "post-terminate"
  | "post-receive";

export type TusdHook = {
  /**
   * The upload's ID. Will be empty during the pre-create event.
   */
  id: string;
  /**
   * The upload's total size in bytes.
   */
  size: number;
  /**
   * The upload's current offset in bytes.
   */
  offset: number;
  /**
   * These properties will be set to true, if the upload as a final or partial one. See the Concatenation extension for details: http://tus.io/protocols/resumable-upload.html#concatenation
   */
  isFinal: boolean;
  /**
   * These properties will be set to true, if the upload as a final or partial one. See the Concatenation extension for details: http://tus.io/protocols/resumable-upload.html#concatenation
   */
  isPartial: boolean;
  /**
   * If the upload is a final one, this value will be an array of upload IDs which are concatenated to produce the upload.
   */
  partialUploads: string[];
  /**
   * The upload's meta data which can be supplied by the clients as it wishes. All keys and values in this object will be strings. Be aware that it may contain maliciously crafted values and you must not trust it without escaping it first!
   */
  metaData: {
    [key: string]: string;
  };
};
